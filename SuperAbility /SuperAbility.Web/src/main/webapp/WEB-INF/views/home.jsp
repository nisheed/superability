<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="false"%>
<html>
<head>
<title><spring:message code="title.Home" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="language" content="english" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link rel="stylesheet" type="text/css" href="resources/css/site.css"
	media="screen, projection" />
<link rel="stylesheet" type="text/css" href="resources/css/homecore.css" />
</head>
<body>
	<div id="topBarContent" class="topBarContent">
		<div class="section">
			<h3><spring:message code="subheading.HomeMain"/></h3>
			<p>
				<spring:message code="content.LoremIpsum"/>
			</p>
		</div>
	</div>
	<div id="sideBarContent" class="sideBarContent">
		
			<h2><spring:message code="subheading.HomeSideBar" /></h2>
			
			<p>
				<spring:message code="content.LoremIpsum"/>
			</p>
		
	</div>
	<div id="coreContent" class="coreContent contain"> 

		
			<h2><spring:message code="subheading.HomeCoreContent" /></h2>
			<p>
				<spring:message code="content.LoremIpsum"/>
			</p>
		
	</div>

</body>
</html>
