<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title><spring:message code="title.SignUp" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="language" content="english" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<link rel="stylesheet" type="text/css" href="resources/css/site.css"
	media="screen, projection" />
<link rel="stylesheet" type="text/css" href="resources/css/homecore.css" />
<jsp:include page="/WEB-INF/views/includes/formCss.jsp" />
<jsp:include page="/WEB-INF/views/includes/jqueryCss.jsp" />
</head>
<body>
	<div id="sideBarContent" class="sideBarContent">

		<img height=250px width=450px
			src="<spring:url value="resources/images/register_image.jpeg" />"
			alt="<spring:message code="image.logo" />" />


	</div>
	<div id="coreContent" class="coreContent contain">
	<span class="form-title">Create an account</span>

			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; (
			or <a href="/Admin/Account/signin">sign in</a> )
		<form:form id="formRegister" method="post"
			modelAttribute="signUpModel" action="signup">

			<div class="editor-label">
				<form:label path="username">
					<spring:message code="label.username" />
					<span class="required">*</span>
				</form:label>
			</div>
			<div class="editor-field">
				<form:input path="username" class="mandatory" />
				<form:errors path="username" class="field-validation-error-left" />
			</div>
			<div class="primaryBtn">
				<input type="submit" value="Create Account" />
			</div>
		</form:form>
	</div>
</body>
</html>