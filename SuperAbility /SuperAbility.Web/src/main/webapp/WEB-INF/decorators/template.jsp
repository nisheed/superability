<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<decorator:head/>
<body>
<div id="canvas">
	<div id="pageWrap">
		<div class="body contain">
			<div id="topBar" class="menu"></div>
			<div class="header contain">
				<p class="logo">
					<a href="/"> <img
						src="<spring:url value="resources/images/logo.png"/>"
						alt="SuperAbility" />
					</a>
				</p>
			</div>
			<div class="logInTools"></div>

		</div>
		<div id="marketingCanvas">
		<decorator:getProperty property="div.topBarContent" />
		<div class="sideBar">
			<decorator:getProperty property="div.sideBarContent" />
		</div>
		<div class="siteContent">
		<decorator:getProperty property="div.coreContent" />
		</div>
		</div>
		<div id="footer" class="footer">
			<div class="footer contain">
				<div class="column">
					<h4><spring:message code="footer.Company" /></h4>
					<ul>
						<li><a href="#"><spring:message code="footer.AboutUs" /></a></li>
						<li><a href="#"><spring:message code="footer.Contact" /></a></li>
					</ul>
				</div>
				<div class="column">
					<h4><spring:message code="footer.FollowUs" /></h4>
					<ul>
						<li class="twitter"><a href="#"><spring:message code="footer.Twitter" /></a></li>
						<li class="facebook"><a href="#"><spring:message code="footer.FaceBook" /></a></li>
					</ul>
				</div>
				<div class="column">
					<h4><spring:message code="footer.Help" /></h4>
					<ul>
						<li><a href="#"><spring:message code="footer.SignUp" /></a></li>
						<li><a href="#"><spring:message code="footer.ForgottenPassword" /></a></li>
					</ul>
				</div>
				<div class="column last">
					<h4><spring:message code="footer.Legal" /></h4>
					<ul>
						<li><a href="#"><spring:message code="footer.TermsAndConditions" /></a></li>
						<li><a href="#"><spring:message code="footer.PrivacyPolicy" /></a></li>
					</ul>
				</div>
				<div class="orgLinks topBrdr contain">
					<p><spring:message code="footer.Copyright" /></p>
				</div>
			</div>
		</div>
	</div>
	</div>
</body>
</html>
