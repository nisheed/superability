/* 
 * Contains all styles for the various modules that sit in the main content area
 */

/*** Form styles ***/

.roundBox {
    background: #eeeae2;
    width: 491px;
    margin: 0 0 10px 0;
}

.roundBox .roundBoxBottom {
    background: url(../images/whiteCornersLrg.png) -691px 100% no-repeat;
    padding: 20px 10px 10px 10px;
}

.roundBox  label, .roundBox .labelFake {
    float: left;
    text-align: right;
    width: 208px;
    color: #000;
    padding: 0 11px 0 0;
}

.roundBox span  a {
    font-size: 86%;
}

.roundBox a:hover {
    color: #00AEEF;
}

.roundBox .required {
    padding: 0 0 0 0.7em;
}

.roundBox .formInfo {
    display: block;
    margin: 0 0 0 220px;
    font-size: 78.60%;
}

.roundBox .cvv {
    width: 100px;
}

.roundBox .termsAndAnchor {
    margin: 0 0 0 220px;
    font-size: 75%;
}

.roundBox .termsAndAnchor input {
    position: relative;
    top: 1px;
}

.roundBox .qMark {
    display: block;
    text-indent: -100000px;
    background: url(../images/qMark.png) 0 0 no-repeat;
    width: 20px;
    height: 20px;
    float: right;
    margin: 0 130px 0 0;
}

.roundBox .qMarkOuter .qMark {
    margin: 0 160px 0 0;
}

.roundBox .addressConfirm span {
    display: block;
}

.roundBox .noReq {
    width: 194px;
    padding-right: 25px;
}

.roundBox .emailReady {
    margin: 0 20px 0 0;
}

.buttons {
    float: right;
}

.buttons p {
    float: left;
    margin: 0 0 0 10px;
}

.buttons .terms a {
    padding: 0 20px;
    line-height: 210%;
    display: block;
}

.buttons .terms a:hover {
    color: #00AEEF;
}

fieldset + p {
    border-bottom: 1px solid #e9e9e9;
    margin: 0 0 20px 0;
}

.newMember h2 {
    background: none;
    color: #000;
    font-weight: bold;
    font-size: 150%;
    padding-left: 0;
}

.newMember h2 + p, .noBg + p {
    border-bottom: 1px solid #e9e9e9;
    margin: 0 0 10px 0;
}

.newMember .primaryBtn {
    float: right;
}

span[id*=errors] {
    background: url(../images/bkErrorAlert.png) 0px 0px no-repeat;
    padding: 2px 0px 0px 24px;
    font-size: 75%;
    color: #cc0000;
     display: block;
    clear: both;
    margin-left: 126px;
}

.inputError {
    border: 1px #cc0000 solid;
}

/*
*	funds
*/

.funds {
    margin-bottom: 6px !important;
}

.funds .offset p {
    font-size: 100%;
    padding: 15px 20px;
}

.funds .offset p strong {
    color: #000;
}

/*
*	Game Rows
*/

.rowOne, .rowTwo, .rowThree, .rowFour {
    margin: 0 0 10px 0;
}

/*
* Chat Module
*/
.chatWrap {
    background: url(../images/chatModule.png) 0 0 no-repeat;
    width: 363px;
    height: 228px;
    position: absolute;
    top: 38%;
    right: 15%;
    padding: 8px 14px;
}

.chatWrap .chatTop h2 {
    float: left;
}

.chatWrap .chatTop .chatClose {
    float: right;
    padding: 0;
    border: 0 none;
}

.chatWrap .chatTop .chatClose a {
    background: url(../images/closeModule.png) 0 0 no-repeat;
    display: block;
    text-indent: -100000px;
    width: 20px;
    height: 18px;
}

.chatWrap .chatInner {
    width: 335px;
    height: 130px;
    margin: 3px 0 9px 0;
    padding: 10px;
}

.chatWrap .chatInner .chatSection {
    width: 220px;
    height: 130px;
}

.chatWrap .chatInner .chatSection p {
    font-size: 100%;
    color: #00aeef;
    padding: 0 0 5px 0;
}

.chatWrap .chatform p {
    float: left;
}

.chatWrap .chatform .primaryBtn {
    float: right;
    margin: 0 6px 0 0;
}

.chatWrap .chatform .primaryBtn input {
    font-weight: bold;
    width: 60px;
    text-align: center;
}


span[id*=place] {
    color: red;
    font-size: 70%;
    display: block;
    clear: both;
    margin-left: 221px;
}

/*
* Progress bar
*/
ul.mainProgressBar {
    text-indent: -999999px;
    list-style: none;
    padding: 0px;
    margin: 0px 0px 20px 0px;
    height: 86px;
}

ul.stepOne {
    background: url(../images/spriteProgressBar.png) no-repeat;
}

ul.stepTwo {
    background: url(../images/spriteProgressBar.png) 0px -89px no-repeat;
}

ul.stepThree {
    background: url(../images/spriteProgressBar.png) 0px -178px no-repeat;
}

/*
 * Transaction tables
 */

.transHistory {
    width: 691px;
    border-collapse: separate;
    border: 1px solid #d7d7d7;
    font-size: 92.3%;
}

.transHistory  thead .date {
    width: 115px;
}

.transHistory  thead .transaction {
    width: 114px;
}

.transHistory  thead .description {
    width: 161px;
}

.transHistory  thead .fundsOut {
    width: 73px;
}

.transHistory  thead .fundsIn {
    width: 68px;
}

.transHistory  thead .ref {
    width: 65px;
}

.transHistory  thead th {
    background: url("../images/bkHeaderFade.png") repeat-x scroll 0 0 #024894;
    padding: 6px 7px 7px 7px;
    font-weight: normal;
    border-left: 1px solid #4189d0;
    border-right: 1px solid #024894;
}

.transHistory  thead th.selected {
    background: #0b4482 url("../images/bkHeaderFade.png") repeat-x scroll 0 0;
}

.transHistory  thead th a {
    background: url("../images/arrowsDown.png") 100% 4px no-repeat;
    float: left;
    display: block;
    color: #fff;
    padding-right: 15px;
}

.transHistory  thead tr th:first-child,
.transHistory  tbody tr td:first-child {
    border-left: 0 none;
}

.transHistory  thead tr th:last-child,
.transHistory  tbody tr td:last-child {
    border-right: 0 none;
}

.transHistory  tbody tr:nth-child(odd) {
    background: #e9f8fe;
}

.transHistory  tbody td {
    border-left: 1px solid #cceffc;
    border-right: 1px solid #fff;
    padding: 6px 7px 7px 7px;
}

/*
* account page_v5
*/

.acMessageWrap {
    background: url("../images/whiteBgLrg.png") -699px 100% no-repeat;
    padding: 0 0 8px;
    margin: 0 0 20px 0;
}

.acMessageWrap ul {
    list-style-type: none;
}

.acMessageWrap .messageList {
    background: url("../images/whiteBgLrg.png") -1398px 100% repeat-y;
    padding: 0;
    margin: 0;
}

.acMessageWrap .greyDate {
    background: #f1f1f1;
    color: #000;
    padding: 4px 0 4px 10px;
    width: 669px;
    margin: 0 0 0 1px;
}

.acMessageWrap .messageList p {
    float: left;
    font-size: 100%;
    padding: 6px 10px;
}

.acMessageWrap .messageInner p {
    padding: 10px;
}

.acMessageWrap .last .messageInner p {
    padding: 10px 10px 5px 10px;
}

.acMessageWrap .messageInner a {
    background: url("../images/envelope.png") 0 0 no-repeat;
    font-weight: bold;
    padding: 0 0 0 26px;
}

.acMessageWrap .messageInner a:hover {
    color: #00AEEF;
}

.acMessageWrap .messageLink {
    width: 200px;
}

/*
* yellow panel
*/

.funds .offset .readyPlay {
    padding: 17px 20px;
}

.funds .offset .primaryBtn {
    padding: 12px 20px;
}

/*
* manage account
*/

.manageAccount {
    background: url("../images/whiteBgLrg.png") -1398px 100% repeat-y;
    padding: 8px 12px;
}

.manageAccount ul {
    border-bottom: 1px solid #e9e9e9;
    margin: 0;
    padding: 0;
}

.manageAccount .last {
    border-bottom: 0 none;
    padding: 0;
}

.manageAccount li {
    float: left;
    width: 30%;
    font-size: 100%;
    padding: 25px 10px;
    height: 51px;
}

.manageAccount li h3 a {
    background: url("../images/manageAccounts.png") no-repeat;
    padding: 10px 0 10px 34px;
}

.manageAccount li h3 a:hover {
    color: #00AEEF;
}

.manageAccount li h3 .personal {
    background-position: 0 -115px;
}

.manageAccount li h3 .security {
    background-position: 0 -177px;
}

.manageAccount li h3 .card {
    background-position: 0 -234px;
}

.manageAccount li h3 .funds {
    background-position: 0 -293px;
}

.manageAccount li h3 .transaction {
    background-position: 0 -361px;
}

.manageAccount li h3 .reminders {
    background-position: 0 3px;
}

.manageAccount li h3 .tickets {
    background-position: 0 -48px;
}

.manageAccount li h3 .personalLimits {
    background-position: 0 -500px;
}

.manageAccount li h3 .saved {
    background-position: 0 -572px;
}

.manageAccount li p {
    margin: 10px 0 0 0;
}

.mainRoundBox {
    background: #ffffff  0 0 no-repeat;
    width: 100%;
    margin: 0 0 20px 0;
    padding: 10px 0px 10px 0px;
}

.mainRoundBox .roundBoxBottom {
    background:  -800px 100% no-repeat;
    padding: 20px 10px 10px 10px;
}

.mainRoundBox  label, .roundBox .labelFake {
    float: left;
    text-align: right;
    width: 120px;
    color: #000;
    padding: 0 11px 0 0;
}

.mainRoundBox span  a {
    font-size: 86%;
}

.mainRoundBox a:hover {
    color: #00AEEF;
}

.mainRoundBox .required {
    padding: 0 0 0 0.7em;
}

.mainRoundBox .formInfo {
    display: block;
    margin: 0 0 0 220px;
    font-size: 78.60%;
}

.mainRoundBox .cvv {
    width: 100px;
}

.mainRoundBox .termsAndAnchor {
    margin: 0 0 0 220px;
    font-size: 75%;
}

.mainRoundBox .termsAndAnchor input {
    position: relative;
    top: 1px;
}

.mainRoundBox .qMark {
    display: block;
    text-indent: -100000px;
    background: url(../images/qMark.png) 0 0 no-repeat;
    width: 20px;
    height: 20px;
    float: right;
    margin: 0 130px 0 0;
}

.mainRoundBox .qMarkOuter .qMark {
    margin: 0 160px 0 0;
}

.mainRoundBox .addressConfirm span {
    display: block;
}

.mainRoundBox .noReq {
    width: 194px;
    padding-right: 25px;
}

.mainRoundBox .emailReady {
    margin: 0 20px 0 0;
} 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 